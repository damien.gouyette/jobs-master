# Technical Assessment

**Context** 

You can develop & refactor your code (using your versioning tool) following this pipeline:

* Download and read the file: product_catalog.csv locally
* Transform the file from CSV to Parquet format locally
* Separate the valid rows from the invalid ones into two separate files: the business wants only the product with an image but wants to archive the invalids rows

# How to install

```
pip install -r requirements.txt 
```

# Run tests

```
python -m unittest tests/*.py
```

# Execute it

```
python -m product_catalog/transformer.py
```